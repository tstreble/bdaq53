#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan records an IV curve of the sensor of a module
    while making sure the chip is powered and configured.
'''

import os
import time
import yaml
import numpy as np
import tables as tb
from tqdm import tqdm

from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

from bdaq53.system import logger
import bdaq53.analysis.analysis_utils as au
from bdaq53.system.periphery import BDAQ53Periphery


scan_configuration = {
    'module_name': 'module_0',
    'hv_current_limit': 1e-6,
    'VBIAS_start': 0,
    'VBIAS_stop': -100,
    'VBIAS_step': -1,

    'samples': 10
}


class RawDataTable(tb.IsDescription):
    voltage = tb.Int32Col(pos=1)
    current = tb.Float64Col(pos=2)
    current_error = tb.Float64Col(pos=3)


class RunConfigTable(tb.IsDescription):
    attribute = tb.StringCol(64)
    value = tb.StringCol(512)


class SensorIVScan(object):
    scan_id = 'sensor_iv_scan'

    def __init__(self, scan_config, bench_config='../testbench.yaml'):
        with open(bench_config, 'r') as f:
            self.bench_config = yaml.safe_load(f)

        self.config = scan_config
        self.run_name = time.strftime("%Y%m%d_%H%M%S") + '_' + self.scan_id
        self.output_filename = os.path.join(self.bench_config['general']['output_directory'], self.config['module_name'], self.run_name)
        self.log = logger.setup_derived_logger(self.__class__.__name__)

        self.periphery = BDAQ53Periphery(bench_config=bench_config)

    def init(self):
        self.periphery.init()
        if not self.periphery.enabled:
            self.periphery.close()
            raise Exception('Periphery module needs to be enabled!')
        if not (self.config['module_name'] in list(self.periphery.module_devices.keys()) and 'HV' in self.periphery.module_devices[self.config['module_name']].keys()):
            self.periphery.close()
            raise Exception('No sensor bias device defined for {}!'.format(self.config['module_name']))

        if not os.path.isdir(os.path.dirname(self.output_filename)):
            os.mkdir(os.path.dirname(self.output_filename))

        self.h5_file = tb.open_file(self.output_filename + '.h5', mode='w', title=self.scan_id)
        self.h5_file.create_group(self.h5_file.root, 'configuration', 'Configuration')
        run_config_table = self.h5_file.create_table(self.h5_file.root.configuration, name='run_config', title='Run config', description=RunConfigTable)
        chip_type = ''
        for k, v in self.bench_config['modules'][self.config['module_name']].items():
            if k not in ['identifier', 'powersupply', 'power_cycle']:
                try:
                    chip_type = v['chip_type'].upper()
                    break
                except KeyError:
                    continue
        for key, value in {'scan_id': self.scan_id, 'run_name': self.run_name, 'module': self.config['module_name'], 'chip_type': chip_type}.items():
            row = run_config_table.row
            row['attribute'] = key
            row['value'] = value
            row.append()
        scan_cfg_table = self.h5_file.create_table(self.h5_file.root.configuration, name='scan_config', title='Scan configuration', description=RunConfigTable)
        for key, value in self.config.items():
            row = scan_cfg_table.row
            row['attribute'] = key
            row['value'] = value
            row.append()
        self.raw_data_table = self.h5_file.create_table(self.h5_file.root, name='raw_data', title='Raw data', description=RawDataTable)

        self.periphery.power_on_LV(self.config['module_name'])

    def __enter__(self):
        self.init()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.periphery.close()

    def start(self):
        try:
            self._scan()
            invert = self.config['VBIAS_stop'] < 0
        finally:
            self.h5_file.close()
            self.periphery.close()
        plot(self.output_filename + '.h5', invert_x=invert)

    def _scan(self):
        '''
        Sensor IV scan main loop

        Parameters
        ----------
        VBIAS_start : int
            First bias voltage to scan
        VBIAS_stop : int
            Last bias voltage to scan. This value is included in the scan.
        VBIAS_step : int
            Stepsize to increase the bias voltage by in every step.
        '''

        module_name = self.config.get('module_name', 'module_0')
        VBIAS_start = self.config.get('VBIAS_start', 0)
        VBIAS_stop = self.config.get('VBIAS_stop', -5)
        VBIAS_step = self.config.get('VBIAS_step', -1)
        hv_current_limit = self.config.get('hv_current_limit', 1e-6)
        samples = self.config.get('samples', 1)

        self.periphery.power_off_HV(module_name)

        try:
            self.periphery.power_on_HV(module_name, hv_voltage=0, hv_current_limit=hv_current_limit)

            add = 1 if VBIAS_stop > 0 else -1
            pbar = tqdm(total=abs(VBIAS_stop), unit='Volt')

            last_v = 0
            for v_bias in range(VBIAS_start, VBIAS_stop + add, VBIAS_step):
                self.periphery.power_on_HV(module_name, hv_voltage=v_bias, hv_current_limit=hv_current_limit, verbose=False)

                # Wait until stable
                last_val = 0
                for _ in range(100):
                    val = float(self.periphery.module_devices[module_name]['HV'].get_current())
                    if abs(last_val - val) < abs(0.1 * val):
                        break
                    last_val = val
                else:
                    self.log.warning('Current is not stabilizing!')
                    break

                # Take 'samples' measurements and use mean as value
                c_arr = []
                for _ in range(samples):
                    c_arr.append(float(self.periphery.module_devices[module_name]['HV'].get_current()))
                    time.sleep(0.1)
                current = np.mean(c_arr)
                row = self.raw_data_table.row
                row['voltage'] = v_bias
                row['current'] = current
                row['current_error'] = np.std(c_arr)
                row.append()
                self.raw_data_table.flush()
                pbar.update(abs(v_bias) - last_v)
                last_v = abs(v_bias)

                # Abort scan if in current limit
                if abs(current) >= hv_current_limit * 0.98:
                    self.log.error('Current limit reached. Aborting scan!')
                    break

            pbar.close()
            self.log.success('Scan finished')
        except Exception as e:
            self.log.error('An error occurred: %s' % e)
        finally:
            self.periphery.power_off_HV(module_name)


def plot(data_file, invert_x=True, log_y=True, level='', text_color='#07529a'):
    with tb.open_file(data_file, 'r') as f:
        data = f.root.raw_data[:]
        run_config = au.ConfigDict(f.root.configuration.run_config[:])

    x, y, yerr = [], [], []
    for d in data:
        x.append(d[0])
        y.append(abs(d[1]))
        yerr.append(d[2])

    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    fig.subplots_adjust(top=0.85)
    y_coord = 0.92

    chip_type = run_config['chip_type']
    fig.text(0.1, y_coord, '{0} {1}'.format(chip_type, level), fontsize=12, color=text_color, transform=fig.transFigure)
    identifier = run_config['module']
    fig.text(0.65, y_coord, 'Module: {0}'.format(identifier), fontsize=12, color=text_color, transform=fig.transFigure)

    ax.errorbar(x, y, yerr=yerr, linestyle='none', marker='.', color='C0')

    ax.set_title('Sensor IV curve', color=text_color)
    ax.set_xlabel('Bias voltage [V]')
    ax.set_ylabel('Leakage current [A]')
    ax.grid()

    if invert_x:
        ax.invert_xaxis()
    if log_y:
        ax.set_yscale('log')

    fig.savefig(data_file[:-3] + '_' + '.pdf')


if __name__ == '__main__':
    with SensorIVScan(scan_config=scan_configuration) as scan:
        scan.start()
