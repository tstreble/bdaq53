#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import unittest
import pytest
import yaml
import numpy as np

import bdaq53
from bdaq53.modules.module_type import ModuleType

bdaq53_path = os.path.dirname(bdaq53.__file__)
modules_path = os.path.abspath(os.path.join(bdaq53_path, 'modules'))


class TestModuleTypes(unittest.TestCase):

    def test_module_types(self):
        ''' Test all module types defined in bdaq53/modules/module_types.yaml '''

        module_types_file = os.path.join(modules_path, 'module_types.yaml')
        with open(module_types_file) as f:
            module_types = yaml.full_load(f)

        # test all module types
        for module_type_name in module_types:
            # class raises ValueError if module type has wrong format
            module_type = ModuleType(module_type_name, "rd53a")

            # do some additional test of module_type functions:
            self.assertTrue(type(module_type.switch_axis()) in [bool, np.bool_])
            self.assertTrue(type(module_type.get_max_corner_size()) in [int, np.int_])
            self.assertTrue(type(module_type.get_low_occupancy(0)) in [list, np.ndarray])
            self.assertTrue(type(module_type.get_chip_ids()) in [list, np.ndarray])
            self.assertTrue(type(module_type.get_chip_config(0)) is dict)
            self.assertTrue(all(x in ['left', 'right', 'top', 'bottom'] for x in module_type.get_big_corners(0)))
            self.assertTrue(len(module_type.get_size(1, 1)) == 2)
            maps = [[np.full((2, 3, 4), sel_id) for sel_id in row] for row in module_type.chip_id_map]
            self.assertTrue(module_type.concatenate_maps(maps).shape[2:] == (4,))
            self.assertTrue(all(key in ['left', 'right', 'top', 'bottom'] and value >= 0
                                for key, value in module_type.get_map_extension().items()))
            self.assertTrue(type(module_type.extend_map(np.zeros((2, 2)))) is np.ndarray)


if __name__ == '__main__':
    pytest.main([__file__])
