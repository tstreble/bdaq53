#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import cocotb
from cocotb.binary import BinaryValue
from cocotb.triggers import RisingEdge, FallingEdge, ReadOnly, Timer
from cocotb.drivers import BusDriver
from cocotb.result import ReturnValue
from cocotb.clock import Clock

from basil.utils.BitLogic import BitLogic
import random
import numpy as np

class HitDataRand(BusDriver):

    _signals = ['CLK_BX', 'HIT', 'TRIGGER', 'RESET', 'RESET_TB']

    def __init__(self, entity, seed=1, start=0, hit_prob=0.0, trig_prob=0.0, stop = 100 ):
        BusDriver.__init__(self, entity, "", entity.CLK_BX)

        val = '0' * len(self.bus.HIT)
        self.hit = BinaryValue(bits=len(self.bus.HIT), value=val )


        self.seed = seed
        self.hit_prob = hit_prob
        self.trig_prob = trig_prob
        self.start = start
        self.stop = stop
        self.dump_hit = np.array([], dtype={'names':['bx','col','row','tot'], 'formats':['uint32','uint8','uint8','uint8']})
        self.dump_trig = np.array([], dtype='uint32')

    @cocotb.coroutine
    def run(self):

        rnd = random.Random()
        rnd.seed(self.seed)

        self.bus.HIT <= self.hit
        self.bus.TRIGGER <= 0

        bx = 0
        '''
        while 1:
            yield RisingEdge(self.clock)
            yield ReadOnly()
            res = self.bus.RESET_TB.value.integer
            if(res == 0):
                break

        while 1:
            yield RisingEdge(self.clock)
            yield ReadOnly()
            res = self.bus.RESET_TB.value.integer
            if(res == 1):
                break
        '''
        bv = BitLogic(len(self.hit))

        while bx < self.stop:
            yield RisingEdge(self.clock)
            yield Timer(15000) #15ns
            if (bx > self.start ):
                bv.setall(False)
                for pix in range(len(self.hit)):
                    if(rnd.random() < self.hit_prob):
                        bv[pix] = '1'
                        #dump = np.array([(bx, pix / 64, pix % 64, 1)], dtype=self.dump_hit.dtype)
                        #self.dump_hit = np.append(self.dump_hit, dump)
                        #print dump

                self.hit.assign(str(bv)); self.bus.HIT <= self.hit

                yield FallingEdge(self.clock)
                if(rnd.random() < self.trig_prob ):
                    self.bus.TRIGGER <= 1
                    #self.dump_trig =  np.append(self.dump_trig, bx)
                else:
                    self.bus.TRIGGER <= 0

            bx += 1

        #clear all bits
        bv.setall(False)
        self.hit.assign(str(bv)); self.bus.HIT <= self.hit

        #print(bv)

            #np.save("dump_hit.npy", self.dump_hit)
            #np.save("dump_trig.npy", self.dump_trig)
