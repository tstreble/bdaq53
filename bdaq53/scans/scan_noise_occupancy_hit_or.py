#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This noise occupancy scan is based on a basic source scan.
    If a pixel exceeds the minimal occupancy during the duration of the source scan it gets masked.
'''

import tables as tb
import numpy as np

from bdaq53.scans.scan_source import SourceScan
from bdaq53.analysis import analysis


scan_configuration = {
    'start_column': 0,        # Start column for mask
    'stop_column': 400,         # Stop column for mask
    'start_row': 0,             # Start row for mask
    'stop_row': 192,            # Stop row for mask

    # Stop conditions (choose one)
    'scan_timeout': 10,          # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time

    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 65,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips/hitors. Should also be adjusted for longer trigger length.

    'use_tdc': False,           # Enable TDC modules

    'hitor_calib_file': None,

    'min_occupancy': 10,         # All pixels with more hits than this threshold are masked as noisy

    # Trigger configuration
    'bdaq': {'TLU': {
        'TRIGGER_MODE': 0,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 1     # Selecting trigger input: HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }
}


class NoiseOccHitOrScan(SourceScan):
    scan_id = 'noise_occupancy_hit_or_scan'

    def _analyze(self):
        super(NoiseOccHitOrScan, self)._analyze()

        min_occupancy = self.configuration['scan'].get('min_occupancy', 1)

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            with tb.open_file(a.analyzed_data_file) as in_file:
                occupancy = in_file.root.HistOcc[:].sum(axis=2)
                disable_mask = np.logical_or(occupancy > min_occupancy, np.logical_not(self.chip.masks['enable']))
        n_disabled_pixels = np.count_nonzero(np.invert(disable_mask))

        self.chip.masks.disable_mask = disable_mask
        self.chip.masks.apply_disable_mask()

        self.log.success('Found and disabled {0} noisy pixels.'.format(n_disabled_pixels))


if __name__ == '__main__':
    with NoiseOccHitOrScan(scan_config=scan_configuration) as scan:
        scan.start()
